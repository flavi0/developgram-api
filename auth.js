'user strict'
/* // es6
import { send, json } from 'micro'
import HttpHash from 'http-hash'
import Db from 'developgram-db'
import DbStub from './test/stub/db'
import config from './config'
import utils from './lib/utils'
*/
var send = require('micro').send
var json  = require('micro').json
var HttpHash = require('http-hash')
var Db = require('developgram-db')
var DbStub = require('./test/stub/db')
var config = require('./config')
var utils = require('./lib/utils')


const env = process.env.NODE_ENV || 'production'
let db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('POST /', async function authentication (req, res, params) {
  console.log('service--auth..')
  const credentials = await json(req)
  console.log('credentials--> ', credentials)
  await db.connect()
  const auth = await db.authenticate(credentials.username, credentials.password)
  await db.disconnect()
  if (!auth) {
    console.log('no auth...')
    return send(res, 401, { error: 'invalid credentials' })
  }
  const token = await utils.signToken({
    userId: credentials.username
  }, config.secret)
  console.log('auth--token-> ', token)
  send(res, 200, token)
})

module.exports = async function main (req, res) {
  const { method, url } = req
  // el match verá si hay alguna ruta con el patron definido
  const match = hash.get(`${method.toUpperCase()} ${url}`)

  // si hay alguna ruta, se retorna un objeto sino null
  if (match.handler) {
    try {
      // ejecutar handler
      await match.handler(req, res, match.params)
    } catch (error) {
      send(res, 500, { error: `Error-server: ${error.message}` })
    }
  } else {
    send(res, 404, { error: 'Ruta no encontrada' })
  }
}
