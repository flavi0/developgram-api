'user strict'
/* // es6
import { send, json } from 'micro'
import HttpHash from 'http-hash'
import Db from 'developgram-db'
// import DbStub from 'proxyRequire' // lo haría automaticamente
import DbStub from './test/stub/db'
import config from './config'
import gravatar from 'gravatar
*/
var send = require('micro').send
var json  = require('micro').json
var HttpHash = require('http-hash')
var Db = require('developgram-db')
// var DbStub = require('proxyRequire' // lo haría automaticamente
var DbStub = require('./test/stub/db')
var config = require('./config')
var gravatar = require('gravatar')

const env = process.env.NODE_ENV || 'production'
let db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('POST /', async function saveUser (req, res, params) {
  const user = await json(req) // obtener usuario de la peticion http
  console.log('env--> ', env)
  console.log('post-user---> ', user)
  if (db) {
    console.log('hay DB')
  } else {
    console.log('NO-hay DB')
  }
  await db.connect()
  const created = await db.saveUser(user)
  await db.disconnect()
  delete created.email
  delete created.password
  send(res, 201, created)
})

hash.set('GET /:username', async function getUser (req, res, params) {
  const username = params.username
  await db.connect()
  const user = await db.getUser(username)
  user.avatar = gravatar.url(user.email)
  const images = await db.getImagesByUser(username)
  user.pictures = images
  delete user.email
  delete user.password
  send(res, 200, user)
})

module.exports = async function main (req, res) {
  const { method, url } = req
  // el match verá si hay alguna ruta con el patron definido
  const match = hash.get(`${method.toUpperCase()} ${url}`)

  // si hay alguna ruta, se retorna un objeto sino null
  if (match.handler) {
    try {
      // ejecutar handler
      await match.handler(req, res, match.params)
    } catch (error) {
      send(res, 500, { error: `Error-server: ${error.message}` })
    }
  } else {
    send(res, 404, { error: 'Ruta no encontrada' })
  }
}
