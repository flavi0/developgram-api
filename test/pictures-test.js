'use strict'

import 'babel-polyfill'
import test from 'ava'
import micro from 'micro'
// import uuid from 'uuid-base62'
import listen from 'test-listen' // testing de microservicios con micro
import request from 'request-promise' // para hacer http-request usando promesas
import pictures from '../pictures'
import fixtures from './fixtures/'
import utils from '../lib/utils'
import config from '../config'

test.beforeEach(async t => {
/* creando servidor con micro
  const srv = micro(async (req, res) => {
    send(res, 200, { id }) // res, status, datoEnviar
  }) */
  const srv = micro(pictures)
  // listen corre el serivdor y retorna url-puerto del servidor
  t.context.url = await listen(srv)
})

test('GET /:id', async t => {
  const image = fixtures.getImage()
  const url = t.context.url
  // haciendo peticion http
  const body = await request({ uri: `${url}/${image.publicId}`, json: true })
  t.deepEqual(body, image, 'body == imagen')
})
// test que faltarian hacer
test('no token POST /', async t => {
  const image = fixtures.getImage()
  const url = t.context.url

  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      description: image.description,
      src: image.src,
      userId: image.userId
    },
    // que devuelva todo, no solo el body
    resolveWithFullResponse: true
  }
  const errorToken = await t.throwsAsync(request(options))
  t.deepEqual(errorToken.error.error, 'invalid token')
})

test('invalida token POST /', async t => {
  const image = fixtures.getImage()
  const url = t.context.url
  // generar jwt
  const token = await utils.signToken({ userId: 'nameHack' }, config.secret)
  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      description: image.description,
      src: image.src,
      userId: image.userId
    },
    headers: {
      'Authorization': `Bearer ${token}` // eslint-disable-line quote-props
    },
    // que devuelva todo, no solo el body
    resolveWithFullResponse: true
  }
  const errorToken = await t.throwsAsync(request(options))
  t.deepEqual(errorToken.error.error, 'invalid token')
})

test('secure POST /', async t => {
  const image = fixtures.getImage()
  const url = t.context.url
  // generar jwt
  const token = await utils.signToken({ userId: image.userId }, config.secret)
  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      description: image.description,
      src: image.src,
      userId: image.userId
    },
    headers: {
      'Authorization': `Bearer ${token}` // eslint-disable-line quote-props
    },
    // que devuelva todo, no solo el body
    resolveWithFullResponse: true
  }
  // const response = await request(options)
  // t.is(response.statusCode, 201, 'request-status es 201')
  // t.deepEqual(response.body, image, 'post: result==image')
  const response = await request(options)
  t.is(response.statusCode, 201)
  t.deepEqual(response.body, image, 'message')
})

test('POST /:id/like', async t => {
  const image = fixtures.getImage()
  const url = t.context.url

  const options = {
    method: 'POST',
    uri: `${url}/${image.id}/like`,
    json: true
  }
  const body = await request(options)
  const imageNew = JSON.parse(JSON.stringify(image)) // forma clonar (no recomendado para objetos grandes)
  imageNew.liked = true
  imageNew.likes = 1
  t.deepEqual(body, imageNew, 'body = imageNew')
})

test('GET /list', async t => {
  const images = fixtures.getImages()
  const url = t.context.url
  const options = {
    metho: 'GET',
    uri: `${url}/list`,
    json: true
  }
  const body = await request(options)
  t.deepEqual(body, images, 'lista de imagenes')
})

test('GET /tag/:tag', async t => {
  const images = fixtures.getImagesByTag()
  const url = t.context.url
  const options = {
    method: 'GET',
    uri: `${url}/tag/genial`,
    json: true
  }
  const body = await request(options)
  t.deepEqual(body, images, 'images por tag')
})
