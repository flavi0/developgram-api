module.exports = {
  getImage () {
    return {
      id: '8f36a148-010f-4154-9428-1006e84d3378',
      publicId: '4meRRrkS5JPo6iR2Pll5tm',
      userId: 'developgram',
      liked: false,
      likes: 0,
      src: 'http://developgram.test/4meRRrkS5JPo6iR2Pll5tm.jpg',
      description: 'foto #genial',
      tags: ['genial'],
      createAt: new Date().toString()
    }
  },
  getImages () {
    return [
      this.getImage(),
      this.getImage(),
      this.getImage()
    ]
  },
  getImagesByTag () {
    return [
      this.getImage(),
      this.getImage()
    ]
  },
  getUser () {
    return {
      id: '8f36a148-010f-4154-9428-1006e84d3378',
      name: 'Flavio Barrantes',
      username: 'flav-io',
      email: 'f.fbs.95.ffbs@gmail.com',
      password: 'password_signup',
      createAt: new Date().toString()
    }
  }
}
