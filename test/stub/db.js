// clase de db 'falsa', una implementación mínima
'user strict'
// import fixtures from '../fixtures'
var fixtures = require ('../fixtures')

module.exports = class Db {
  connect () {
    return Promise.resolve(true) // para que sea asincrono
  }

  disconnect () {
    return Promise.resolve(true)
  }

  getImage (id) {
    return Promise.resolve(fixtures.getImage())
  }

  saveImage (id) {
    return Promise.resolve(fixtures.getImage())
  }

  likeImage (id) {
    const image = fixtures.getImage()
    image.liked = true
    image.likes = 1
    return Promise.resolve(image)
  }

  getImages () {
    return Promise.resolve([
      fixtures.getImage(),
      fixtures.getImage(),
      fixtures.getImage()
    ])
  }

  getImagesByTag (tag) {
    return Promise.resolve(fixtures.getImagesByTag())
  }

  saveUser (user) {
    return Promise.resolve(fixtures.getUser())
  }

  getUser (username) {
    return Promise.resolve(fixtures.getUser())
  }

  authenticate (username, password) {
    return Promise.resolve(true)
  }
}
