'use strict'

import 'babel-polyfill'
import test from 'ava'
import micro from 'micro'
// import uuid from 'uuid-base62'
import listen from 'test-listen' // testing de microservicios con micro
import request from 'request-promise' // para hacer http-request usando promesas
import users from '../users'
import fixtures from './fixtures'

test.beforeEach(async t => {
  const srv = micro(users)
  t.context.url = await listen(srv)
})

test('POST /', async t => {
  const user = fixtures.getUser()
  const url = t.context.url
  const options = {
    method: 'POST',
    uri: url,
    json: true,
    body: {
      name: user.username,
      password: user.password,
      email: user.email
    },
    resolveWithFullResponse: true
  }
  const response = await request(options)

  delete user.email
  delete user.password

  t.is(response.statusCode, 201, 'status = 201')
  t.deepEqual(response.body, user)
})

test('GET /:username', async t => {
  const user = fixtures.getUser()
  const url = t.context.url
  const options = {
    method: 'GET',
    uri: `${url}/${user.username}`,
    json: true
  }
  const body = await request(options)
  delete user.email
  delete user.password

  t.deepEqual(body, user)
})
