'use strict'

import 'babel-polyfill'
import test from 'ava'
import micro from 'micro'
// import uuid from 'uuid-base62'
import listen from 'test-listen' // testing de microservicios con micro
import request from 'request-promise' // para hacer http-request usando promesas
import auth from '../auth'
import fixtures from './fixtures'
import utils from '../lib/utils'
import config from '../config'

test.beforeEach(async t => {
  const srv = micro(auth)
  t.context.url = await listen(srv)
})

test('success POST /', async t => {
  const user = fixtures.getUser()
  const url = t.context.url
  const options = {
    method: 'POST',
    uri: url,
    body: {
      username: user.username,
      password: user.password
    },
    json: true
  }
  const token = await request(options)
  const decoded = await utils.verifyToken(token, config.secret)
  t.is(decoded.userId, user.username, 'usuario autenticado')
})
