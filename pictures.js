'user strict'
/* // es6
import { send, json } from 'micro'
import HttpHash from 'http-hash'
import Db from 'developgram-db'
// import DbStub from 'proxyRequire' // lo haría automaticamente
import DbStub from './test/stub/db'
import config from './config'
import utils from './lib/utils'
*/
var send = require('micro').send
var HttpHash = require('http-hash')
var json = require('micro').json
var Db = require('developgram-db')
var DbStub = require('./test/stub/db')
var config = require('./config')
var utils = require('./lib/utils')

const env = process.env.NODE_ENV || 'production'
let db = new Db(config.db)

if (env === 'test') {
  db = new DbStub()
}

const hash = HttpHash()

hash.set('GET /tag/:tag', async function byTag (req, res, params) {
  const tag = params.tag
  await db.connect()
  const images = await db.getImagesByTag(tag)
  await db.disconnect()
  send(res, 200, images)
})

hash.set('GET /list', async function list (req, res, params) {
  console.log('api-list: ', params)
  await db.connect()
  const images = await db.getImages()
  await db.disconnect()
  console.log('api-list-images: ', images)
  send(res, 200, images)
})

// definiendo ruta
hash.set('GET /:id', async function getPicture (req, res, params) {
  const id = params.id
  await db.connect()
  const image = await db.getImage(id)
  await db.disconnect()
  // enviar la imagen en la respuesta
  send(res, 200, image)
})

hash.set('POST /', async function postPicture (req, res, params) {
  console.log('api--pictures...')
  const image = await json(req)
  // verificar token
  try {
    const token = await utils.extractToken(req)
    const encoded = await utils.verifyToken(token, config.secret)
    // verificar que el usuario que quiere guardar la imagen
    // es el usuario que está en el token
    if (encoded && encoded.userId !== image.userId) {
      throw new Error('invalid token')
    }
  } catch (error) {
    return send(res, 401, { error: 'invalid token' })
  }
  await db.connect()
  const created = await db.saveImage(image)
  await db.disconnect()
  send(res, 201, created)
})

hash.set('POST /:id/like', async function likePicture (req, res, params) {
  const id = params.id
  await db.connect()
  const image = await db.likeImage(id)
  await db.disconnect()
  send(res, 200, image)
})

// export default
module.exports = async function main (req, res) {
  const { method, url } = req
  // el match verá si hay alguna ruta con el patron definido
  const match = hash.get(`${method.toUpperCase()} ${url}`)

  // si hay alguna ruta, se retorna un objeto sino null
  if (match.handler) {
    try {
      // ejecutar handler
      await match.handler(req, res, match.params)
    } catch (error) {
      send(res, 500, { error: `Error-server: ${error.message}` })
    }
  } else {
    send(res, 404, { error: 'Ruta no encontrada' })
  }
}
