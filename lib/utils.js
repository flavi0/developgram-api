'user strict'
/* // es6
import jwt from 'jsonwebtoken'
import bearer from 'token-extractor'
*/
var jwt = require('jsonwebtoken')
var bearer = require('token-extractor')

// libreria para autorizacion de token
module.exports = {
  async signToken (payload, secret, options) {
    // firmar un token
    return new Promise((resolve, reject) => {
      jwt.sign(payload, secret, options, (err, token) => {
        if (err) return reject(err)
        resolve(token)
      })
    })
  },

  async verifyToken (token, secret, options) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, secret, options, (err, decoded) => {
        if (err) return reject(err)

        resolve(decoded) // devuelve el payload decodificado
      })
    })
  },

  async extractToken (req) {
    // obtener el request del campo del header y parsear
    // formato del header:
    // Authorization: Bearer <token>
    return new Promise((resolve, reject) => {
      // extrae el token del request
      bearer(req, (err, token) => {
        if (err) return reject(err)
        resolve(token)
      })
    })
  }
}
